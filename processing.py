import pandas as pd
import categoricals as cat
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import scale

categorical_col_dtype_dict = {
    "MSZoning": cat.MSZoning,
    "Street": cat.Street,
    "Alley": cat.Alley,
    "LotShape": cat.LotShape,
    "LandContour": cat.LandContour,
    "Utilities": cat.Utilities,
    "LotConfig": cat.LotConfig,
    "LandSlope": cat.LandSlope,
    "Neighborhood": cat.Neighborhood,
    "Condition1": cat.Condition1,
    "Condition2": cat.Condition2,
    "BldgType": cat.BldgType,
    "HouseStyle": cat.HouseStyle,
    "RoofStyle": cat.RoofStyle,
    "RoofMatl": cat.RoofMatl,
    "Exterior1st": cat.Exterior1st,
    "Exterior2nd": cat.Exterior2nd,
    "MasVnrType": cat.MasVnrType,
    "ExterQual": cat.ExterQual,
    "ExterCond": cat.ExterCond,
    "Foundation": cat.Foundation,
    "BsmtQual": cat.BsmtQual,
    "BsmtCond": cat.BsmtCond,
    "BsmtExposure": cat.BsmtExposure,
    "BsmtFinType1": cat.BsmtFinType1,
    "BsmtFinType2": cat.BsmtFinType2,
    "Heating": cat.Heating,
    "HeatingQC": cat.HeatingQC,
    "CentralAir": cat.CentralAir,
    "Electrical": cat.Electrical,
    "KitchenQual": cat.KitchenQual,
    "Functional": cat.Functional,
    "FireplaceQu": cat.FireplaceQu,
    "GarageType": cat.GarageType,
    "GarageFinish": cat.GarageFinish,
    "GarageQual": cat.GarageQual,
    "GarageCond": cat.GarageCond,
    "PavedDrive": cat.PavedDrive,
    "PoolQC": cat.PoolQC,
    "Fence": cat.Fence,
    "MiscFeature": cat.MiscFeature,
    "SaleType": cat.SaleType,
    "SaleCondition": cat.SaleCondition
}


def load_price_data(path):
    df = pd.read_csv(
        path,
        index_col='Id',
        dtype=categorical_col_dtype_dict,
        na_values="NA",
        keep_default_na=False
    )

    df.update(df.select_dtypes(include=[np.number]).fillna(0))
    df = pd.get_dummies(df, dummy_na=True)

    return df


def predict_and_save(estimator, X, file_path):
    pred_array = estimator.predict(X)
    pred_series = pd.Series(pred_array, index=X.index, name="SalePrice")

    pred_series.to_csv(file_path, index=True, header=True)


def custom_score(y, y_pred, **kwargs):
    return mean_squared_error(np.log(y), np.log(y_pred))


def select_non_categories(X):
    return X.select_dtypes(exclude=['uint8'])


def get_scaled_df(X):
    non_category_df = select_non_categories(X)
    scaled_non_category_cols = pd.DataFrame(scale(non_category_df), columns=non_category_df.columns)

    df_copy = X.copy()
    df_copy.update(scaled_non_category_cols)
    return df_copy

