import pandas as pd

MSZoning = pd.CategoricalDtype(categories=[
    'A',
    'C (all)',
    'FV',
    'I',
    'RH',
    'RL',
    'RP',
    'RM'
])

Street = pd.CategoricalDtype(categories=[
    'Grvl',
    'Pave'
])

Alley = pd.CategoricalDtype(categories=[
    'Grvl',
    'Pave',
])

LotShape = pd.CategoricalDtype(categories=[
    'Reg',
    'IR1',
    'IR2',
    'IR3'
])

LandContour = pd.CategoricalDtype(categories=[
    'Lvl',
    'Bnk',
    'HLS',
    'Low'
])

Utilities = pd.CategoricalDtype(categories=[
    'AllPub',
    'NoSewr',
    'NoSeWa',
    'ELO'
])

LotConfig = pd.CategoricalDtype(categories=[
    'Inside',
    'Corner',
    'CulDSac',
    'FR2',
    'FR3'
])

LandSlope = pd.CategoricalDtype(categories=[
    'Gtl',
    'Mod',
    'Sev'
])

Neighborhood = pd.CategoricalDtype(categories=[
    'Blmngtn',
    'Blueste',
    'BrDale',
    'BrkSide',
    'ClearCr',
    'CollgCr',
    'Crawfor',
    'Edwards',
    'Gilbert',
    'IDOTRR',
    'MeadowV',
    'Mitchel',
    'NAmes',
    'NoRidge',
    'NPkVill',
    'NridgHt',
    'NWAmes',
    'OldTown',
    'SWISU',
    'Sawyer',
    'SawyerW',
    'Somerst',
    'StoneBr',
    'Timber',
    'Veenker'
])

Condition1 = pd.CategoricalDtype(categories=[
    'Artery',
    'Feedr',
    'Norm',
    'RRNn',
    'RRAn',
    'PosN',
    'PosA',
    'RRNe',
    'RRAe'
])

Condition2 = pd.CategoricalDtype(categories=[
    'Artery',
    'Feedr',
    'Norm',
    'RRNn',
    'RRAn',
    'PosN',
    'PosA',
    'RRNe',
    'RRAe'
])

BldgType = pd.CategoricalDtype(categories=[
    '1Fam',
    '2fmCon',
    'Duplex',
    'TwnhsE',
    'Twnhs'
])

HouseStyle = pd.CategoricalDtype(categories=[
    '1Story',
    '1.5Fin',
    '1.5Unf',
    '2Story',
    '2.5Fin',
    '2.5Unf',
    'SFoyer',
    'SLvl'
])

RoofStyle = pd.CategoricalDtype(categories=[
	'Flat',
	'Gable',
	'Gambrel',
	'Hip',
	'Mansard',
	'Shed'
])

RoofMatl = pd.CategoricalDtype(categories=[
	'ClyTile',
	'CompShg',
	'Membran',
	'Metal',
	'Roll',
	'Tar&Grv',
	'WdShake',
	'WdShngl'
])

Exterior1st = pd.CategoricalDtype(categories=[
	'AsbShng',
	'AsphShn',
	'BrkComm',
	'BrkFace',
	'CBlock',
	'CemntBd',
	'HdBoard',
	'ImStucc',
	'MetalSd',
	'Other',
	'Plywood',
	'PreCast',
	'Stone',
	'Stucco',
	'VinylSd',
	'Wd Sdng',
	'WdShing'
])

Exterior2nd = pd.CategoricalDtype(categories=[
	'AsbShng',
	'AsphShn',
	'Brk Cmn',
	'BrkFace',
	'CBlock',
	'CmentBd',
	'HdBoard',
	'ImStucc',
	'MetalSd',
	'Other',
	'Plywood',
	'PreCast',
	'Stone',
	'Stucco',
	'VinylSd',
	'Wd Sdng',
	'Wd Shng'
])

MasVnrType = pd.CategoricalDtype(categories=[
	'BrkCmn',
	'BrkFace',
	'CBlock',
	'None',
	'Stone'
])

ExterQual = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po'
])

ExterCond = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po'
])

Foundation = pd.CategoricalDtype(categories=[
	'BrkTil',
	'CBlock',
	'PConc',
	'Slab',
	'Stone',
	'Wood'
])

BsmtQual = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po',
])

BsmtCond = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po',
])

BsmtExposure = pd.CategoricalDtype(categories=[
	'Gd',
	'Av',
	'Mn',
	'No',
])

BsmtFinType1 = pd.CategoricalDtype(categories=[
	'GLQ',
	'ALQ',
	'BLQ',
	'Rec',
	'LwQ',
	'Unf',
])

BsmtFinType2 = pd.CategoricalDtype(categories=[
	'GLQ',
	'ALQ',
	'BLQ',
	'Rec',
	'LwQ',
	'Unf',
])

Heating = pd.CategoricalDtype(categories=[
	'Floor',
	'GasA',
	'GasW',
	'Grav',
	'OthW',
	'Wall'
])

HeatingQC = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po'
])

CentralAir = pd.CategoricalDtype(categories=[
	'N',
	'Y'
])

Electrical = pd.CategoricalDtype(categories=[
	'SBrkr',
	'FuseA',
	'FuseF',
	'FuseP',
	'Mix'
])

KitchenQual = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po'
])

Functional = pd.CategoricalDtype(categories=[
	'Typ',
	'Min1',
	'Min2',
	'Mod',
	'Maj1',
	'Maj2',
	'Sev',
	'Sal'
])

FireplaceQu = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po',
])

GarageType = pd.CategoricalDtype(categories=[
	'2Types',
	'Attchd',
	'Basment',
	'BuiltIn',
	'CarPort',
	'Detchd',
])

GarageFinish = pd.CategoricalDtype(categories=[
	'Fin',
	'RFn',
	'Unf',
])

GarageQual = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po',
])

GarageCond = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
	'Po',
])

PavedDrive = pd.CategoricalDtype(categories=[
	'Y',
	'P',
	'N'
])

PoolQC = pd.CategoricalDtype(categories=[
	'Ex',
	'Gd',
	'TA',
	'Fa',
])

Fence = pd.CategoricalDtype(categories=[
	'GdPrv',
	'MnPrv',
	'GdWo',
	'MnWw',
])

MiscFeature = pd.CategoricalDtype(categories=[
	'Elev',
	'Gar2',
	'Othr',
	'Shed',
	'TenC',
])

SaleType = pd.CategoricalDtype(categories=[
	'WD',
	'CWD',
	'VWD',
	'New',
	'COD',
	'Con',
	'ConLw',
	'ConLI',
	'ConLD',
	'Oth'
])

SaleCondition = pd.CategoricalDtype(categories=[
	'Normal',
	'Abnorml',
	'AdjLand',
	'Alloca',
	'Family',
	'Partial'
])


